import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Scanner;
public class LeitorCsv {
    private File pastaPendentes;
    private File pastaValidados;
    private File pastaInvalidos;

    public LeitorCsv(String pastaPendentes, String pastaValidados, String pastaInvalidos){
        this.pastaPendentes = new File(pastaPendentes);
        this.pastaValidados = new File(pastaValidados);
        this.pastaInvalidos = new File(pastaInvalidos);

    }

    public void executarLeitura(){
        criarPastaSeNaoExistir(pastaValidados);
        criarPastaSeNaoExistir(pastaInvalidos);

        File[] listaArquivos = pastaPendentes.listFiles();
        if(listaArquivos != null){
            for(File arquivo : listaArquivos){
                if(arquivo.isFile() && arquivo.getName().endsWith(".csv")){
                    processarArquivoCSV(arquivo);
                }
            }
        }
    }

    public void criarPastaSeNaoExistir(File pasta){
        if(!pasta.exists()){
            pasta.mkdirs();
            System.out.println("Pasta " + pasta.getName() + " criada em " + pasta.getAbsolutePath());
        }
    }

    public void processarArquivoCSV(File arquivo) {
        try {
            if (verificarArquivoCSV(arquivo)) {
                moverArquivo(arquivo, pastaValidados);

            } else {
                moverArquivo(arquivo, pastaInvalidos);

            }
        } catch (IOException e) {
            System.out.println("Erro ao mover o arquivo: " + arquivo.getName());
            e.printStackTrace();
        }
    }

    public boolean verificarArquivoCSV(File arquivo) throws FileNotFoundException {
        String[] colunasEsperadas = {"NUMERO_DA_VENDA", "NOME_DO_CLIENTE", "DATA_DA_VENDA", "VALOR_DA_VENDA"};

        try (Scanner scanner = new Scanner(arquivo)) {
            if (scanner.hasNextLine()) {
                String cabecalho = scanner.nextLine();
                String[] colunas = cabecalho.split(";");

                if (colunas.length == colunasEsperadas.length) {
                    for (int i = 0; i < colunas.length; i++) {
                        if (!colunas[i].trim().equals(colunasEsperadas[i])) {
                            return false;
                        }
                    }
                    while (scanner.hasNextLine()) {
                        String linhaDados = scanner.nextLine();
                        String[] dados = linhaDados.split(";");
                        if (dados.length != colunasEsperadas.length) {
                            return false;
                        }

                        try {
                            Integer.parseInt(dados[0].trim());
                        } catch (NumberFormatException e) {
                            return false;
                        }
                    }

                    return true;
                }
            }
        }

        return false;
    }



    public void moverArquivo(File arquivo, File destino) throws IOException {
        Path origem = arquivo.toPath(); // convertendo um objeto arquivo em um Path que representa diretorio
        Path destinoPath = destino.toPath().resolve(arquivo.getName());
        Files.move(origem, destinoPath, StandardCopyOption.REPLACE_EXISTING);
        System.out.println("Arquivo movido para: " + destinoPath);
    }

    public void imprimirArquivosPasta(File pasta) {
        System.out.println("============================");
        System.out.println("Arquivos da pasta: " + pasta.getName());
        File[] arquivos = pasta.listFiles();
        if (arquivos != null) {
            for (File arquivo : arquivos) {
                System.out.println(arquivo.getName());
            }
        }
    }

    public File getPastaPendentes() {
        return pastaPendentes;
    }

    public void setPastaPendentes(File pastaPendentes) {
        this.pastaPendentes = pastaPendentes;
    }
    public File getPastaValidados() {
        return pastaValidados;
    }

    public void setPastaValidados(File pastaValidados) {
        this.pastaValidados = pastaValidados;
    }

    public File getPastaInvalidos() {
        return pastaInvalidos;
    }

    public void setPastaInvalidos(File pastaInvalidos) {
        this.pastaInvalidos = pastaInvalidos;
    }
}
