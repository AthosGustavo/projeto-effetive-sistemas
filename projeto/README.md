## Importante
Tentei importar o arquivo JAR diversas vezes no ecllipse,mas não consegui.Olhei diversas soluções no youtube e continuou aprensentando erros, devido a isso
optei pelo intellij e o erro foi corrigido.



## Instruções para executar o programa
1-Crie 3 variáveis Strings para guardar o diretório das pastas PENDENTES,VALIDADOS e  INVALIDADOS.Troque o diretório padrão pelo diretório do seu computador.

2-A pasta pendente deve ser criada manualmente e deve conter os arquivos.csv

3-Nas variáveis das pastas VALIDADOS e INVALIDOS digite o diretório da pasta ja existente ou o diretório que você deseja criar as pastas.Ex:a pasta ainda não existe: C:\Users\athos\Desktop\ArquivosCSV/VALIDADOS

4-Troque as barras `\\` do diretório por `/`

5-Instancie um objeto LeitorCsv passando as variáveis de diretorio no construtor

6-Execute o método principal `executarLeitura` para fazer a distribuição de arquivos.

7-Para testar o método executarLeitura() pela segunda vez,é necessário mover os arquivos da pasta VALIDADOS e INVALIDADOS para PENDENTES

8-Como importar o JAR no Intellij:https://www.youtube.com/watch?v=Fg-8ondcZNM&t=199s	
