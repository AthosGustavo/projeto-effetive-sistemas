import java.io.File;
import java.io.IOException;

public class LeitorCsvApp {

    public static void main(String[] args) {
        String pastaPendentes = "C:/Users/athos/Desktop/teste pastas/PENDENTES";
        String pastaValidados = "C:/Users/athos/Desktop/teste pastas/VALIDADOS";
        String pastaInvalidos = "C:/Users/athos/Desktop/teste pastas/INVALIDOS";

        LeitorCsv leitor = new LeitorCsv(pastaPendentes, pastaValidados, pastaInvalidos);
        leitor.executarLeitura();

         //Teste método moverArquivo
//       String nomeArquivo = "arquivo1.csv";
//       File arquivoUm = new File(pastaValidados, nomeArquivo);
//
//       try {
//          leitor.moverArquivo(arquivoUm, leitor.getPastaPendentes());
//
//       }catch (IOException e){
//           System.out.println("Erro ao mover o arquivo: " + e.getMessage());
//       }

        // Imprimindo os arquivos contidos nas pastas
        File arquivosPastaValidados = new File(pastaValidados);
        leitor.imprimirArquivosPasta(arquivosPastaValidados);

        File arquivosPastaInvalidados = new File(pastaInvalidos);
        leitor.imprimirArquivosPasta(arquivosPastaInvalidados);

        File arquivosPastaPendentes = new File(pastaPendentes);
        leitor.imprimirArquivosPasta(arquivosPastaPendentes);
    }
}
